package manifoldplayground.extensions.java.util.List;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Extension
public class ListExt {
    public static <E> List<E> filter(@This List<E> thiz, Predicate<E> filter) {
        return thiz.stream()
            .filter(filter)
            .collect(toList());
    }

    public static <E> List<E> plus(@This List<E> left, List<E> right) {
        List<E> sum = new ArrayList<>(left.size() + right.size());
        sum.addAll(left);
        sum.addAll(right);
        return sum;
    }
}