package org.example;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.example.Unit.mm;

public class App {
    public static void main(String[] args) {
        User captain = User.builder()
            .withFirstName("Jack")
            .withLastName("Sparrow")
            .build();
        System.out.println(captain.write().toJson());

        "Hello ${captain.getFirstName()}".println();

        List<Integer> even = asList(2, 4, 6, 8, 10);
        List<Integer> odd = asList(1, 3, 5, 7, 9);
        List<Integer> numbers = even + odd;
        assert asList(2, 4, 6, 8, 10, 1, 3, 5, 7, 9).equals(numbers);

        List<Integer> evenNumbers = numbers.filter(i -> i % 2 == 0);
        assert asList(2, 4, 6, 8, 10).equals(evenNumbers);

        float dtp = 210 mm;
        System.out.println("210 mm are $dtp dtp");

    }
}
