package org.example;

public enum Unit {
    mm;

    public float postfixBind(int millimeter) {
        return (float) (millimeter / 25.4d * 72d);
    }

}
